# Functionality of ecan.map
# (1) make simple maps
# (2) allow colour variations
# (3) plot event data
# (4) plot bathymetry with chosen isobaths
# (5) be able to colour isobaths
# (6) add legends
# *(7) be able to determine location of events inside a series a polygons
# (8) add event data with bubble sizes according to magnitude
# *(9) determine area of polygons
# (10) include NAFO areas and other areas as datafiles
# (11) allow importation of bathymetry for other areas
#
# *maybe not, these are GIS functions
#
# Functions
# basic map given lats and longs, allow colours for land and sea, map with land features like lakes and rivers
# add place names
# add bathymetry
# add events, point types, labels
# add polygons
# add trace e.g. trawl trace given start and end points. Be able to choose line type and arrow directions and head and tail type. allow colour gradient along line (segments function)
#   arguments are the start and end position, colour start and colour end,
# add statistical areas
# convert dd2dms and vice versa
# import bathymetry for other areas
# concave and convex hull around a cluster of points

# Statistical areas needed: Scotian shelf trawl, georges trawl, fishing areas (scallop, lobster, crab, clam, shrimp), MPA, EBSA, sponge,
# sea pens,

#' Create a map. Default coordinates centre the map of the Gulf of St. Lawrence
#'
#' @param longs longitude (x) in decimal degrees from prime meridian. West values are -
#' @param lats latitude (y) in decimal degrees from the equator South values are -
#' @param land.colour colour of the land. Default is "tan"
#' @param sea.colour colour of the sea. Default is rgb(224,253,254,maxColorValue=255): a light blue
#' @param xlab Label for the x-axis (longitude)
#' @param ylab Label for the y-axis (latitude)
#' @description  Draws a map given lat and long coordinates. Colours for land and sea can be changed.
#'               Maps spanning the prime meridian may not appear centred as you want them. See refocusWorld.
#' @author Daniel Duplisea
#' @seealso plotMap, refocusWorld
#' @return
#' @export
#' @examples
#' map.f()
#' map.f(longs=c(-65,-40), lats=c(42,65), land.colour="slategrey",sea.colour="lightblue")
map.f= function(longs=c(-71,-50),lats=c(43,52.25),land.colour="tan",sea.colour=rgb(224,253,254,maxColorValue=255),
                xlab="Longitude West", ylab="Latitude North"){
  land="tan"
  sea=rgb(224,253,254,maxColorValue=255)
  library(PBSmapping)
  data(worldLLhigh)
  worldLLhigh$X=(worldLLhigh$X+180)%%360-180
  xlim=longs
  ylim=lats
  map.data=clipPolys(worldLLhigh,xlim=xlim,ylim=ylim)
  plotMap(map.data,xlim=xlim,ylim=ylim,lty=1,lwd=.05 ,col=land.colour,
          bg=sea.colour,las=1,xaxt="n", yaxt="n",
          xlab="",ylab="")
  xint= seq(longs[1],longs[2],length=5)
  yint= seq(lats[1],lats[2],length=6)
  mtext(xlab,side=1,line=3)
  mtext(ylab,side=2,line=3)
  axis(1, at=xint, labels=xint*-1, lty=1,lwd=1,lwd.ticks= 1, cex.axis=.7)
  axis(2, at=yint, labels=yint*1, lty=1,lwd=1,lwd.ticks=1,las=1, cex.axis=.7)
  box()
}

#' Show statistical areas and boundaries on your map
#'
#' @param stat.area the name of the statistical area you want to draw (string). See names(statistical.areas) for those available. By default it draws the multispecies trawl survey strata for nGSL
#' @param strata.colour base colour of the strata
#' @param border.colour border colour of the strata lines
#' @param highlight.stratum highlight particular strata corresponding to those in uniques(...$PID). Where ... is your stat area
#' @param highlight.colour colour of the highlighted strata, one colour or a vector. Recycled
#' @param show.only.highlight T/F if T will show only the highlighted regions and none of the others
#' @param lty type of line to delineate the strata
#' @description Show your chosen statistical area on the map. Individual polygons can be highlighted with a different colour (recycled).
#'              The highlighted polygon must have an entry as a PID in your stat.area.
#' @author Daniel Duplisea
#' @return
#' @export
#' @examples
#' map.f(); stat.area.f("ngsl.trawl.survey.strata")
#' map.f(lats=c(40,60)); stat.area.f("NL.trawl.survey.strata",strata.colour="lightblue",border.colour="black",
#'      highlight.strata=c(41,275), highlight.colour=c("red","green"))
#' map.f(lats=c(40,60)); stat.area.f("NL.trawl.survey.strata",strata.colour="lightblue",border.colour="black",
#'      highlight.strata=c(41,275), highlight.colour=c("red","yellow"), show.only.highlight=T)
#' unique(statistical.areas$ngsl.trawl.survey.strata$PID) # to find the listing of the strata designations for ngsl
stat.area.f= function(stat.area="ngsl.trawl.survey.strata", strata.colour= rgb(224,253,254,maxColorValue=255), border.colour="grey", highlight.strata="none",
                       highlight.colour="red", show.only.highlight=F, lty=1){
  location= paste0("statistical.areas$",stat.area)
  statistical.area= eval(str2expression(location))
  strata.id= unique(statistical.area$PID)
  if(show.only.highlight){
    statistical.area= statistical.area[statistical.area$PID %in% highlight.strata,]
    strata.id= unique(statistical.area$PID) #need a new id.vector because set has been reduced
  }
  if(is.numeric(highlight.strata)){
      strata.colour.vector= rep(strata.colour, length=length(strata.id))
      strata.colour.vector[strata.id %in% highlight.strata]= highlight.colour
      addPolys(statistical.area,border=border.colour, col=strata.colour.vector)
    }
  else addPolys(statistical.area,border=border.colour, col=strata.colour)
}
# load("/home/daniel/bin/PBSmapping/Strates/NGSL strata borders.rda")
#save(strat.N,file="/home/daniel/github/ecan.map/data/strat.ngulf.rda")
#save(strat.S,file="/home/daniel/github/ecan.map/data/strat.sgulf.rda")
#strat.NL= importShapefile("/home/daniel/database/strata/fweastcoastsurveystratumdefinitionfile_rda/NF_SamplingStrata_20140514.shp")
#save(strat.NL,file="/home/daniel/github/ecan.map/data/strat.NL.rda")
# descriptions= read.csv2("/home/daniel/github/ecan.map/extra/statistical.area.descriptions.csv", sep=";")
# statistical.areas=
#   list(description= descriptions,
#        ngsl.trawl.survey.strata=strat.N,
#        sgsl.trawl.survey.strata=strat.S,
#        NL.trawl.survey.strata=strat.NL,
#        gsl.EAR= EAR.gulf)
# save(statistical.areas,file="/home/daniel/github/ecan.map/data/statistical.areas.rda")


#' Show Ecosystem Approach Regions for the Gulf of St. Lawrence, clipped to a minimum depth
#' @param min.isobath the minimum depth that you want the regions to be extended to. Default is 30m
#' @param strata.colour base colour of the strata
#' @param border.colour border colour of the strata lines
#' @param highlight.stratum highlight particular strata corresponding to those in unique(strat.NL$PID)
#' @param highlight.colour colour of the highlighted strata, one colour or a vector. Recycled
#' @param lty type of line to delineate the strata
#' @description Show survey strata for the Newfoundland stratified random trawl survey on the map.
#'              Individual strata can be highlighted with a different colour (recycled). The highlighted
#'              strata must have an entry in strat.NL$PID.
#' @author Daniel Duplisea
#' @seealso gslea
#' @return
#' @export
#' @examples
#' map.f(); EcoAppRegions.f(min.isobath=30, strata.colour="orange", border.colour="black")
EcoAppRegions.f= function(min.isobath=30, strata.colour= rgb(224,253,254,maxColorValue=255), border.colour="grey", highlight.strata="none",
                       highlight.colour="red", lty=1){
  clipped= clip.regions.to.bathy.f(EcoAppRegions, min.isobath)
  if(is.numeric(highlight.strata)){
      strata.id= unique(clipped$PID)
      strata.colour.vector= rep(strata.colour, length=length(strata.id))
      strata.colour.vector[strata.id %in% highlight.strata]= highlight.colour
      addPolys(clipped,border=border.colour, col=strata.colour.vector)
    }
  else addPolys(clipped,border=border.colour, col=strata.colour)
}

# save(EcoAppRegions, file="/home/daniel/github/ecan.map/data/EcoAppRegions.rda")


#load("//home/daniel/github/ccca/data/ecan.bathy.rda")
#ECan.bathymetry= ocBathy
#save(ECan.bathymetry,file="/home/daniel/github/ecan.map/data/ECan.bathymetry.rda")
#' Draw bathymetric lines on a map of eastern Canada
#'
#' @param isobaths the isobaths that you want to show on the map (m)
#' @param isob.col colours of the isobaths shown on the map. Recycled.
#' @param legend show a legend on the map. TRUE or FALSE.
#' @param legend.pos position of the legend, e.g. "topright". see ?legend
#' @param legend.cex character expansion for legend text. Default 0.7 (70% = 30% reduction)
#' @description Draws a legend for the bathymetry on your map which you can position in different spots on the map
#' @author Daniel Duplisea borrowing code from Rowan Haigh and Denis Chabot
#' @return
#' @export
#'
#' @examples
#' map.f(); bathymetry.f(legend.pos="topleft")
bathymetry.f= function(isobaths= c(200,300), isob.col= c("blue","slategrey"), legend=T, legend.pos="bottomleft", legend.cex=0.7){
    bathycontour = contourLines(ECan.bathymetry, levels=isobaths)
    bathypoly = convCP(bathycontour, projection = "LL")
    bathypoly2 = bathypoly$PolySet
    addLines(thinPolys(bathypoly2, tol=1,filter = 5), col =isob.col)
    if(legend){
      legend(legend.pos, bty = "n", col = isob.col, lwd = 1, legend = as.character(isobaths), inset = 0.05,
         title = "Isobaths (m)",cex=legend.cex)
    }
}



#' Draw tracks on the map with colour gradient line. e.g. for a trawl survey
#'
#' @param xstart starting x (longitude) position of the track. Decimal-degrees
#' @param ystart starting y (latitude) position of the track. Decimal-degrees
#' @param xend ending x (longitude) position of the track. Decimal-degrees
#' @param yend ending y (latitude) position of the track. Decimal-degrees
#' @param start.colour starting colour of the track line
#' @param end.colour ending colour of the track line
#' @param number.of.colours number of colours to interpolate between start and end colours. Use 1 for a single colour. Use a high number (100) for a smoother gradient
#' @param ... anything to the function segments
#' @description Draws a track event data that has starting and ending positions. The line can have a colour gradient
#' @author Daniel Duplisea
#' @return
#' @export
#' @examples
#' tmp= data.frame(xstart= runif(10,-62,-60),ystart= runif(10,47.5,48.5)); tmp$xend=tmp$xstart*1.01; tmp$yend=tmp$ystart*1.01 #makeup fake tracks
#' map.f(); tracks.f(tmp$xstart, tmp$ystart, tmp$xend, tmp$yend,start.colour="red", end.colour="blue", number.of.colours=30)
#' track.arrows.f(tmp$xstart, tmp$ystart, tmp$xend, tmp$yend, arr.lwd=.06, arr.length=0.15, arr.col="blue", col="blue", arr.type="triangle")
tracks.f= function(xstart, ystart, xend, yend, start.colour, end.colour, number.of.colours=1, ...){
  if(any(is.na(c(xstart,xend,ystart,yend)))) stop("NA values are not allowed in starting or ending values")
  cols= colorRampPalette(c(start.colour, end.colour))(number.of.colours)
  for(i in 1:length(xstart)){
      xline= seq(xstart[i], xend[i], length=number.of.colours+1)
      yline= seq(ystart[i], yend[i], length=number.of.colours+1)
      #points(xline, yline, col=cols, pch=20, ...)
      #lines(xline, yline, col=cols, pch=20, ...)
      #segments(xline, yline, col=cols, pch=20, ...)
      segments(x0= head(xline,-1),
               y0= head(yline,-1),
               x1= tail(xline,-1),
               y1= tail(yline,-1),
               col=cols, ...)
  }
}

#' Put arrowheads on the tracks of trawl surveys or any directional point data (start and end values)
#'
#' @param xstart starting x (longitude) position of the track
#' @param ystart starting y (latitude) position of the track
#' @param xend ending x (longitude) position of the track
#' @param yend end y (latitude) position of the track
#' @param ... anything from shape::Arrows()
#' @description a wrapper for Karline Soetaert's Arrow and Arrowhead functions from the package shape. Shape is
#'              shaoe is not a dependency here, Soetaert's functions have been copied into this library.
#'              Soetaert's functions have many options for arrowhead types, colours and sizes that can be used here.
#' @seealso shape::Arrows, shape::Arrowhead
#' @author Karline Soetaert <karline.soetaert@nioz.nl> (borrowed from shape) Daniel Duplisea (wrapper)
#' @return
#' @export
#' @examples
#' tmp= data.frame(xstart= runif(10,-62,-60),ystart= runif(10,47.5,48.5)); tmp$xend=tmp$xstart*1.01; tmp$yend=tmp$ystart*1.01 #create fake trawl tracks to plot
#' map.f(); track.arrows.f(tmp$xstart, tmp$ystart, tmp$xend, tmp$yend)
track.arrows.f= function(xstart, ystart, xend, yend, ...){
  #uses Arrows from shape
  if(any(is.na(c(xstart,xend,ystart,yend)))) stop("NA values are not allowed in starting or ending values")
  KSArrows(xstart,ystart,xend,yend, ...)
}


# havesine distance. Package procma
#Hans W. Borchers

#haveshine distance between start and end points of the trawl track (in kilometres). Coorindates in decimal-degrees
#' Havershine distance between start and end points of the trawl track
#'
#' @param xstart starting x (longitude) position of the track. Decimal-degrees
#' @param ystart starting y (latitude) position of the track. Decimal-degrees
#' @param xend ending x (longitude) position of the track. Decimal-degrees
#' @param yend ending y (latitude) position of the track. Decimal-degrees
#' @param R radius of the earth (6371 km)
#' @description computes the distance between two geographic points on a globe (km). Vectorised
#' @author Daniel Duplisea took the equation from procma::haveshine distance (Hans Borchers)
#' @return
#' @export
#' @examples
#' hv.distance.f(-63,44,-68,48.5)
hv.distance.f= function(xstart,ystart,xend,yend,R=6371){
  xstart= xstart*pi/180
  ystart= ystart*pi/180
  xend= xend*pi/180
  yend= yend*pi/180
  delta.x= xend - xstart
  delta.y= yend - ystart
  inter= sin(delta.x/2)^2 + cos(xstart) * cos(xend) * sin(delta.y/2)^2
  inter2= 2*atan2(sqrt(inter), sqrt(1 - inter))
  dist= R*inter2
  dist
}





#' Clip polygons to bathymetric areas
#'
#' @param toclip the polyset you want to clip to the bathymetric isobath.
#' @param isobath the isobaths that you want to clip the region to
#' @description Clips a polyset region (e.g. NAFO regions) to a bathymetric depth contour. For example, most of the survey do not
#'              survey in waters shallower than 50m so it can be slightly deceptive to depict them as the whole surveyed area. This
#'              is apparent around the Magdalen Island, for example. The area of the 'suitable' depths may also be a better description
#'              of potential habitat.
#' @author Daniel Duplisea
#' @return
#' @export
#' @examples
#' map.f(); addPolys(EcoAppRegions) #contrast this with the EcoAppRegions clipped to the 50m contour (next example)
#' map.f(); addPolys(clip.regions.to.bathy.f(EcoAppRegions, 50))
clip.regions.to.bathy.f= function(toclip, isobath= 5){
    bathycontour = contourLines(ECan.bathymetry, levels=isobath)
    bathypoly = convCP(bathycontour, projection = "LL")
    bathypoly2 = bathypoly$PolySet
    reduced.bathypolyset= thinPolys(bathypoly2, tol=2,filter = 20)
    clipped= joinPolys(toclip,reduced.bathypolyset,operation="DIFF")
    clipped
}
# EcoAppRegions50= clip.regions.to.bathy.f(EcoAppRegions, 50)
# save(EcoAppRegions50, file="/home/daniel/github/ecan.map/data/EcoAppRegions50.rda")
#map.f()
#EcoAppRegions.f(min.isobath=50, strata.colour="orange", border.colour="black")
#calcArea(clip.regions.to.bathy.f(EcoAppRegions, 50))


decdeg= function(degmin.sec){
  # convert geographic coordinates in degree minutes seconds into decimal decgrees
  # in one number e.g. 44 38' 52", input as 443852
  deg= floor(degmin.sec/10000)
  min= floor((degmin.sec - deg*10000)/100)/60
  decimal.remainder=10*degmin.sec%%1
  #sec= (degminsec-100*floor(degminsec/100))/3600
  #dd= deg+min+decimal.remainder
  dd= deg+min+decimal.remainder
  dd
}



old.track.arrows.f= function(xstart, ystart, xend, yend, len=20, angle=20, ...){
  if(any(is.na(c(xstart,xend,ystart,yend)))) stop("NA values are not allowed in starting or ending values")
  for(i in 1:length(xstart)){
      xline= seq(xstart[i], xend[i], length=1000)
      yline= seq(ystart[i], yend[i], length=1000)
      arrows(tail(xline,2)[1],tail(yline,2)[1],tail(xline,1),tail(yline,1), len=len, angle=angle, ...)
  }
}

# from the library shape.
# Author
# Karline Soetaert <karline.soetaert@nioz.nl>
# put KS in front so as to not mask Arrowhead from shape
# I have set the default for segment=F. Because I have separated the drawing of the track from the drawing of the arrowhead to allow a colour
# gradient in the line for the survey track
KSArrowhead= function (x0, y0, angle = 0, arr.length = 0.4, arr.width = arr.length/2,
    arr.adj = 0.5, arr.type = "curved", lcol = "black", lty = 1,
    arr.col = lcol, arr.lwd = 2, npoint = 5, ...)
{
    if (arr.type == "none") {
        return()
    }
    if (arr.type == "curved") {
        rad <- 0.7
        len <- 0.25 * pi
        mid <- c(0, rad)
        x <- seq(1.5 * pi + len, 1.5 * pi, length.out = npoint)
        rr <- cbind(mid[1] - rad * cos(x), mid[2] + rad * sin(x))
        mid <- c(0, -rad)
        x <- rev(x)
        rr <- rbind(rr, cbind(mid[1] - rad * cos(x), mid[2] -
            rad * sin(x)))
        mid <- c(rr[nrow(rr), 1], 0)
        rd <- rr[1, 2]
        x <- seq(pi/2, 3 * pi/2, length.out = 3 * npoint)
        rr <- rbind(rr, cbind(mid[1] - rd * 0.25 * cos(x), mid[2] -
            rd * sin(x)))
        rr[, 1] <- rr[, 1] * 2.6
        rr[, 2] <- rr[, 2] * 3.45
    }
    else if (arr.type == "triangle") {
        x <- c(-0.2, 0, -0.2)
        y <- c(-0.1, 0, 0.1)
        rr <- 6.22 * cbind(x, y)
    }
    else if (arr.type %in% c("circle", "ellipse")) {
        if (arr.type == "circle")
            arr.width = arr.length
        rad <- 0.1
        mid <- c(-rad, 0)
        x <- seq(0, 2 * pi, length.out = 15 * npoint)
        rr <- 6.22 * cbind(mid[1] + rad * sin(x), mid[2] + rad *
            cos(x))
    }
    if (arr.adj == 0.5)
        rr[, 1] <- rr[, 1] - min(rr[, 1])/2
    if (arr.adj == 0)
        rr[, 1] <- rr[, 1] - min(rr[, 1])
    user <- par("usr")
    pcm <- par("pin") * 2.54
    sy <- (user[4] - user[3])/pcm[2]
    sx <- (user[2] - user[1])/pcm[1]
    nr <- max(length(x0), length(y0), length(angle), length(arr.length),
        length(arr.width), length(lcol), length(lty), length(arr.col))
    if (nr > 1) {
        x0 <- rep(x0, length.out = nr)
        y0 <- rep(y0, length.out = nr)
        angle <- rep(angle, length.out = nr)
        arr.length <- rep(arr.length, length.out = nr)
        arr.width <- rep(arr.width, length.out = nr)
        lcol <- rep(lcol, length.out = nr)
        lty <- rep(lty, length.out = nr)
        arr.col <- rep(arr.col, length.out = nr)
    }
    RR <- rr
    for (i in 1:nr) {
        dx <- rr[, 1] * arr.length[i]
        dy <- rr[, 2] * arr.width[i]
        angpi <- angle[i]/180 * pi
        cosa <- cos(angpi)
        sina <- sin(angpi)
        RR[, 1] <- cosa * dx - sina * dy
        RR[, 2] <- sina * dx + cosa * dy
        RR[, 1] <- x0[i] + RR[, 1] * sx
        RR[, 2] <- y0[i] + RR[, 2] * sy
        polygon(RR, col = arr.col[i], border = lcol[i], lty = lty[i],
            lwd = arr.lwd, ...)
    }
}

KSArrows= function (x0, y0, x1, y1, code = 2, arr.length = 0.4, arr.width = arr.length/2,
    arr.adj = 0.5, arr.type = "curved", segment = FALSE, col = "black",
    lcol = col, lty = 1, arr.col = lcol, lwd = 1, arr.lwd = lwd,
    ...)
{
    if (arr.type == "simple") {
        arrows(x0, y0, x1, y1, code = code, length = arr.length/2.54,
            lty = lty, col = col, lwd = lwd, ...)
        return()
    }
    if (arr.type == "none") {
        return()
    }
    if (arr.type == "T") {
        arrows(x0, y0, x1, y1, code = code, length = arr.length/(2 *
            2.54), lty = lty, angle = 90, col = col, lwd = lwd,
            ...)
        return()
    }
    if (segment)
        segments(x0, y0, x1, y1, col = lcol, lty = lty, lwd = lwd,
            ...)
    user <- par("usr")
    pin <- par("pin")
    pin <- pin/max(pin)
    sy <- (user[4] - user[3])/pin[2]
    sx <- (user[2] - user[1])/pin[1]
    angle <- atan((y1 - y0)/(x1 - x0) * sx/sy)/pi * 180
    angle[is.nan(angle)] <- 0
    angle[x1 < x0] <- 180 + angle[x1 < x0]
    xx <- x1
    yy <- y1
    if (sy < 0 & sx < 0)
        angle <- angle + 180
    else if (sx < 0)
        angle <- angle + 180
    if (code == 3)
        KSArrowhead(x0 = xx, y0 = yy, angle = angle, lcol = lcol,
            arr.col = arr.col, arr.adj = arr.adj, lty = lty,
            arr.length = arr.length, arr.width = arr.width, arr.type = arr.type,
            arr.lwd = arr.lwd, ...)
    if (code != 2) {
        angle <- 180 + angle
        xx <- x0
        yy <- y0
    }
    KSArrowhead(x0 = xx, y0 = yy, angle = angle, lcol = lcol, arr.col = arr.col,
        arr.adj = arr.adj, lty = lty, arr.length = arr.length,
        arr.width = arr.width, arr.type = arr.type, arr.lwd = arr.lwd,
        ...)
}

# consider ditching this one
map.gshhg.f= function(longs=c(360-71,360-50),lats=c(43,52.25),land.colour="sienna3",sea.colour="lightblue"){
  limits= list(x = longs, y = lats)
  polys= importGSHHS("/home/daniel/bin/PBSmapping/gshhg/gshhs_i.b", xlim = limits$x, limits$y, maxLevel = 4)  # automatically converts to ??W
  polys.PD = attributes(polys)$PolyData
  plotMap(polys,xlim=longs,ylim=lats,lty=1,lwd=.05 ,col="tan",
          bg=rgb(224,253,254,maxColorValue=255),las=1,xaxt="n", yaxt="n",
          xlab="",ylab="")
  xint= seq(longs[1],longs[2],length=6)
  yint= seq(lats[1],lats[2],length=6)
  mtext("Longitude west",side=1,line=3)
  mtext("Latitude north",side=2,line=4)
  axis(1, at=xint, labels=xint*-1, lty=1,lwd=1,lwd.ticks= 1, cex.axis=.7)
  axis(2, at=yint, labels=yint*1, lty=1,lwd=1,lwd.ticks=1,las=1, cex.axis=.7)
}

# consider ditching this too
map.gshhg.rivers.f= function(longs=c(360-71,360-50),lats=c(43,52.25),land.colour="sienna3",sea.colour="lightblue"){
  library(PBSmapping)
  limits <- list(x = longs, y = lats)
  polys <- importGSHHS("/home/daniel/bin/PBSmapping/gshhg/wdb_rivers_i.b", xlim = limits$x, limits$y, maxLevel = 4)  # automatically converts to ??W
  polys.PD = attributes(polys)$PolyData
  plotMap(polys,xlim=longs,ylim=lats,lty=1,lwd=.05 ,col="tan",
          bg=rgb(224,253,254,maxColorValue=255),las=1,xaxt="n", yaxt="n",
          xlab="",ylab="")
  xint= seq(longs[1],longs[2],length=6)
  yint= seq(lats[1],lats[2],length=6)
  mtext("Longitude west",side=1,line=3)
  mtext("Latitude north",side=2,line=4)
  axis(1, at=xint, labels=xint*-1, lty=1,lwd=1,lwd.ticks= 1, cex.axis=.7)
  axis(2, at=yint, labels=yint*1, lty=1,lwd=1,lwd.ticks=1,las=1, cex.axis=.7)
}


# longs=c(0,360); lats=c(43,52.25)
# limits <- list(x = longs, y = lats)
# tmp <- importGSHHS("/home/daniel/bin/PBSmapping/gshhg/wdb_rivers_i.b", xlim = limits$x, limits$y, maxLevel = 4)  # automatically converts to ??W
# tmp2= refocusWorld(tmp,xlim=c(360-71,360-50),ylim=c(43,52.25))
# map.f()
# plotMap(worldLLhigh,xlim=c(360-71,360-50),ylim=c(43,52.25))
# addPolys(tmp2)
